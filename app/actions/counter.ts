export const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
export const DECREMENT_COUNTER = 'DECREMENT_COUNTER';

export function increment(): any {
    return {
        type: INCREMENT_COUNTER
    };
}

export function decrement(): any {
    return {
        type: DECREMENT_COUNTER
    };
}

export function incrementIfOdd(): any {
    return (dispatch, getState) => {
        const { counter } = getState();

        if (counter % 2 === 0) {
            return;
        }

        dispatch(increment());
    };
}

export function incrementAsync(delay = 1000): any {
    return dispatch => {
        setTimeout(() => {
            dispatch(increment());
        }, delay);
    };
}
