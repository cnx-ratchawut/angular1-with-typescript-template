import { SET_FULLNAME, INITIAL } from '../actions/navbar';

export default function navbar(state = INITIAL, action): any {
    switch (action.type) {
        case SET_FULLNAME:
            return action.payload;
        default:
            return state;
    }
}
