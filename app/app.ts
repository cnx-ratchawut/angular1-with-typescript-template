// app.ts
import { module, element, bootstrap, ILogService } from 'angular';
import '@uirouter/angularjs';
import 'kendo-ui-core';
import 'rx-angular';
import ngRedux from 'ng-redux';
import thunk from 'redux-thunk';

import { AppComponent } from '../app/app.component';
import { HomeComponent } from '../app/home/home.component';
import { UserComponent } from '../app/user/user.component';
import { UserService } from '../app/services/user.services';
import { AppRoute } from './app-routes';
import './app.less';
import './kendo.less';
// import './app.scss';

import { BindComponent, ChildComponent } from './components/bind.component';
import { rootReducer } from './reducers/rootReducer';
import counter from './directives/counter.directive';
import { NavbarComponent } from './core/navbar.component';
import { RoadShowDetailComponent } from './components/roadshow-detail/roadshow-detail.component';
import { RoadShowComponent } from './components/roadshow/roadshow.component';
import { MeetingSummaryComponent } from './components/meeting-summary/metting-summary.component';
import { EventTargetComponent } from './components/event-target/event-target.component';
// import { default as DevTools, runDevTools } from './devTools';

export let app = module('app', [
    'ui.router',
    'rx',
    ngRedux
])
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$ngReduxProvider', ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider, $locationProvider: any, $ngReduxProvider: ngRedux.INgReduxProvider) => {
        $ngReduxProvider.createStoreWith(rootReducer, [thunk]);
        $locationProvider.hashPrefix('');
        AppRoute($stateProvider);
        $urlRouterProvider.otherwise('/');
    }])
    .component(AppComponent.NAME, new AppComponent())
    .component(HomeComponent.NAME, new HomeComponent())
    .component(UserComponent.NAME, new UserComponent())
    .component(BindComponent.NAME, new BindComponent())
    .component(RoadShowComponent.NAME, new RoadShowComponent())
    .component(MeetingSummaryComponent.NAME, new MeetingSummaryComponent())
    .component(RoadShowDetailComponent.NAME, new RoadShowDetailComponent())
    .component(EventTargetComponent.NAME, new EventTargetComponent())
    .component(NavbarComponent.NAME, new NavbarComponent())
    .directive('ngrCounter', counter)
    .component(ChildComponent.NAME, new ChildComponent())
    .service(UserService.NAME, UserService);
element(document).ready(() => {
    bootstrap(document, ['app']);
});
