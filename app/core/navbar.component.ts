import * as NavActions from '../actions/navbar';
import ngRedux from 'ng-redux';

class NavbarController implements ng.IController {
    static $inject = ['$scope', '$ngRedux'];
    constructor(public $scope: any, public $ngRedux: ngRedux.INgRedux) {
        // this.$ngRedux.subscribe(() => {
        //     let state = this.$ngRedux.getState();
        //     console.log(state);
        // });
        const unsubscribe = $ngRedux.connect(this.mapStateToThis, NavActions)(this);
        $scope.$on('$destroy', unsubscribe);
    }
    mapStateToThis(state): any {
        return {
            fullName: state.navbar,
        };
    }
}

export class NavbarComponent implements ng.IComponentOptions {
    static NAME: string = 'navbarView';
    controller: any;
    //controllerAs: string = 'vm';
    template: any;
    bindings: any;
    constructor() {
        this.controller = NavbarController;
        this.template = `
        <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="logoText">Coporate Access</a>
                <button ng-click="$ctrl.setFullname('123')">set</button>
            </div>
            <div class="col-md-6">
                <ul class="list-inline text-right headerProfile">
                    <li class="list-inline-item">
                        <a href="#">Jarayuth Phromtha {{$ctrl.fullName }} </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#" class="bookmarkicon">
                    <i class="fa fa-bookmark"></i></a></li>
                </ul>
            </div>
        </div>
    </div>`;
    }
}
