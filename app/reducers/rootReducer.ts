import { combineReducers } from 'redux';
import counter from './counter';
import navbar from './navbar';
export const rootReducer = combineReducers({
  counter, navbar
});
