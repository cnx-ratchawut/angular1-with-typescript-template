class RoadShowDetailController implements ng.IController {
    bindCount: number = 0;
    static $inject = ['$ngRedux'];
    constructor(public $ngRedux: any) {
        this.$ngRedux.subscribe(() => {
            let state = this.$ngRedux.getState();
            console.log(state);
        });
    }
}

export class RoadShowDetailComponent implements ng.IComponentOptions {
    static NAME: string = 'roadShowDetailView';
    controller: any;
    controllerAs: string = 'vm';
    template: any;
    bindings: any;
    constructor() {
        this.controller = RoadShowDetailController;
        this.template = `
        roadShowDetailView
        `;
    }
}
