export class UserService {
  static $inject = ['$q', '$http', '$location'];
  static NAME: string = 'userService';
  constructor(protected $q: ng.IQService, protected $http: ng.IHttpService, protected $location: ng.ILocationService) { }
  public getAll(): angular.IHttpPromise<any> {
    return this.$http.get('/api/SampleData/WeatherForecasts');
  }
  public call(): Rx.Observable<any> {
    const api = this.$http.get('https://gbxtradingmobile.firebaseio.com/.json');
    return Rx.Observable.fromPromise(api);
  }
}
