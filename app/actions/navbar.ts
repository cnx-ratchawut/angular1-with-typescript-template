export const SET_FULLNAME = 'SET_FULLNAME';
export const INITIAL = '-';
export function setFullname(fullname): any {
    return {
        type: SET_FULLNAME,
        payload: fullname
    };
}
