class RoadShowController implements ng.IController {
    bindCount: number = 0;
    static $inject = ['$ngRedux'];
    constructor(public $ngRedux: any) {
        this.$ngRedux.subscribe(() => {
            let state = this.$ngRedux.getState();
            console.log(state);
        });
    }
}

export class RoadShowComponent implements ng.IComponentOptions {
    static NAME: string = 'roadShowView';
    controller: any;
    controllerAs: string = 'vm';
    template: any;
    bindings: any;
    constructor() {
        this.controller = RoadShowController;
        this.template = `
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active"
                        id="main-tab" data-toggle="tab"
                        role="tab" aria-controls="main"
                        ui-sref="roadshow.detail"
                        aria-selected="true">Roadshow detail</a>
                </li>
                <li class="nav-item">
                <a class="nav-link active"
                    data-toggle="tab"
                    role="tab"
                    ui-sref="roadshow.event-target"
                    aria-selected="true">Day event and client target list details
                </a>
            </li>
                <li class="nav-item">
                    <a class="nav-link active"
                        data-toggle="tab"
                        role="tab"
                        ui-sref="roadshow.meeting"
                        aria-selected="true">Meeting summary
                    </a>
                </li>
            </ul>
            <ui-view></ui-view>
        `;
    }
}
