class EventTargetController implements ng.IController {
    bindCount: number = 0;
    static $inject = ['$ngRedux'];
    constructor(public $ngRedux: any) {
        this.$ngRedux.subscribe(() => {
            let state = this.$ngRedux.getState();
            console.log(state);
        });
    }
}

export class EventTargetComponent implements ng.IComponentOptions {
    static NAME: string = 'eventTargetView';
    controller: any;
    controllerAs: string = 'vm';
    template: any;
    bindings: any;
    constructor() {
        this.controller = EventTargetController;
        this.template = `
        eventTargetView
        `;
    }
}
