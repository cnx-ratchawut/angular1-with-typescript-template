import { UserService } from '../services/user.services';
class BindController implements ng.IController {
    bindCount: number = 0;
    callFunction: () => any;
    static $inject = ['userService', '$ngRedux'];
    click(): void {
        this.callFunction();
    }
    constructor(public userService: UserService, public $ngRedux: any) {
        userService.call().subscribe(response => {
            console.log(response.data);
        });
        console.log('called');
        this.$ngRedux.subscribe(() => {
            let state = this.$ngRedux.getState();
            console.log(state);
        });
    }
}

export class BindComponent implements ng.IComponentOptions {
    static NAME: string = 'bindView';
    controller: any;
    //controllerAs: string = 'vm';
    template: any;
    bindings: any;
    constructor() {
        this.controller = BindController;
        this.template = `
        <h3>Filter component</h3>
        <a class='btn btn-default btn-sm' ng-click='$ctrl.click()'>ROOT</a>
        <span data-ng-bind='$ctrl.childMessage'></span>
        <child-view call-function='$ctrl.callFunction'></child-view >
        `;
    }
}
class ChildController implements ng.IController {
    public count: number;
    callFunction: any;
    constructor() {
        this.count = 0;
    }
    increase(): void {
        this.count++;
    }
    $onInit(): void {
        let vm = this;
        this.callFunction = function (): void {
            vm.count++;
        };
    }
}

export class ChildComponent implements ng.IComponentOptions {
    static NAME: string = 'childView';
    controller: any;
    // controllerAs: string = 'vm';
    template: any;
    bindings: any;

    constructor() {
        this.controller = ChildController;
        this.bindings = {
            callFunction: '='
        };
        this.template = `
            <h4>Child Component</h4>
            <p> Save Count = {{ $ctrl.count }}
            <hr/>
            <a class='btn btn-default btn-sm' ng-click='$ctrl.increase()'>CHILD</a>

        `;
    }
}
