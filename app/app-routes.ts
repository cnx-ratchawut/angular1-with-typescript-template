import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { RoadShowDetailComponent } from './components/roadshow-detail/roadshow-detail.component';
import { RoadShowComponent } from './components/roadshow/roadshow.component';
import { MeetingSummaryComponent } from './components/meeting-summary/metting-summary.component';
import { EventTargetComponent } from './components/event-target/event-target.component';

export const AppRoute = function ($stateProvider: ng.ui.IStateProvider): any {
    $stateProvider.state({
        name: 'app',
        url: '/',
        redirectTo: '/roadshow',
        component: RoadShowComponent.NAME
    }).state({
        name: 'home',
        url: '/home',
        component: HomeComponent.NAME
    }).state({
        name: 'user',
        url: '/user?id',
        component: UserComponent.NAME,
    }).state({
        name: 'roadshow',
        url: '/roadshow',
        component: RoadShowComponent.NAME
    }).state({
        name: 'roadshow.detail',
        url: '/detail',
        component: RoadShowDetailComponent.NAME
    }).state({
        name: 'roadshow.meeting',
        url: '/meeting',
        component: MeetingSummaryComponent.NAME
    }).state({
        name: 'roadshow.event-target',
        url: '/event-target',
        component: EventTargetComponent.NAME
    });
};
