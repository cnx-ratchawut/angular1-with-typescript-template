import { UserService } from '../services/user.services';
import * as $ from 'jquery';
import * as kendo from 'kendo-ui-core';
/// <reference path='kendo.all.d.ts' />

class UserController implements ng.IController {
    static $inject = ['userService', '$state'];
    users: any;
    constructor(public user: UserService, public $state: ng.ui.IStateService) {
        this.initUI();
    }
    onchange(event: Event): void {
    }
    dropdownOption: kendo.ui.DropDownListOptions = {
        dataTextField: 'text',
        dataValueField: 'value',
        dataSource: new kendo.data.DataSource({
            data: [{ text: 'Anumart', value: 'Ton' }]
        }),
        index: 0,
        change: onchange
    };
    initUI(): void {
        $('#dropdown').kendoDropDownList(this.dropdownOption);
    }
}
export class UserComponent implements ng.IComponentOptions {
    static NAME: string = 'userView';
    controller: any;
    templateUrl: any;
    constructor() {
        this.controller = UserController;
        this.templateUrl = require('./user.html');
    }
}
