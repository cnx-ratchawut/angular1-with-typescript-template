class MeetingSummaryController implements ng.IController {
    bindCount: number = 0;
    static $inject = ['$ngRedux'];
    constructor(public $ngRedux: any) {
        this.$ngRedux.subscribe(() => {
            let state = this.$ngRedux.getState();
            console.log(state);
        });
    }
}

export class MeetingSummaryComponent implements ng.IComponentOptions {
    static NAME: string = 'meetingSummaryView';
    controller: any;
    controllerAs: string = 'vm';
    template: any;
    bindings: any;
    constructor() {
        this.controller = MeetingSummaryController;
        this.template = `
        meetingSummaryView
        `;
    }
}
