import * as CounterActions from '../actions/counter';

export default function counter(): ng.IDirective {
    return {
        restrict: 'E',
        controllerAs: 'counter',
        controller: CounterController,
        scope: {
            diName: '@'
        },
        template: `
        <div>
            <pre>{{ diName ||'is null' }} </pre>
            <p>Clicked: {{counter.value}} times </p>
            <button ng-click='counter.increment()'>+</button>
            <button ng-click='counter.decrement()'>-</button>
            <button ng-click='counter.incrementIfOdd()'>Increment if odd</button>
            <button ng-click='counter.incrementAsync()'>Increment Async</button>
        </div>`,
    };
}

class CounterController {

    constructor($ngRedux, $scope) {
        const unsubscribe = $ngRedux.connect(this.mapStateToThis, CounterActions)(this);
        $scope.$on('$destroy', unsubscribe);
        console.log($scope);
    }
    mapStateToThis(state): any {
        return {
            value: state.counter,
        };
    }
}
